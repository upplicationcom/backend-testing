package com.upplication.training.testing;

import java.math.BigDecimal;

public class TaxCalculator {

    /**
     * Given a price (with tax included) and the tax, the method return the price
     * without the tax and two decimal
     *
     * @param priceWithTax BigDecimal price with the tax included
     * @param tax          BigDecimal tax %
     * @return BigDecimal with the price without the tax and two decimal rounded
     */
    public BigDecimal getPriceWithoutTax(BigDecimal priceWithTax, BigDecimal tax) {
        return new BigDecimal(0);
    }

    /**
     * Given a price without the tax included and the tax, the method return the price with tax
     * and two decimal
     *
     * @param priceWithoutTax BigDecimal price without the tax included
     * @param tax             BigDecimal tax %
     * @return BigDecimal with the price with the tax and two decimal rounded.
     * If the tax is null, then return priceWithoutTax without any modifications.
     */
    public BigDecimal getPriceWithTax(BigDecimal priceWithoutTax, BigDecimal tax) {
        return new BigDecimal(0);
    }
}