package com.upplication.training.testing;

import java.math.BigDecimal;

public class RoundUtil {

    /**
     * Given a double, the method return the double value with two decimals
     *
     * @param number double mandatory
     * @return double rounded
     */
    public double round(double number) {
        return 0;
    }

    /**
     * Given a BigDecimal the method return a BigDecimal with two decimals
     *
     * @param number BigDecimal mandatory
     * @return BigDecimal rounded
     */
    public BigDecimal round(BigDecimal number) {
        return new BigDecimal(0);
    }
}