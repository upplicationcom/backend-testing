package com.upplication.training.testing;


import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * We want to build a Tax Calculator and a RoundUtil
 * Fix the tests without changing the test.
 */
public class TaxCalculatorTest {

    @Test
    public void given_price_with_tax_included_then_we_want_the_price_without_the_tax() {
        TaxCalculator taxCalculator = new TaxCalculator();

        BigDecimal result = taxCalculator.getPriceWithoutTax(new BigDecimal(100), new BigDecimal(10));

        assertEquals(90.90909091, result.doubleValue(), 0);
    }

    @Test
    public void given_price_with_tax_included_then_we_want_the_price_without_the_tax_case2() {
        TaxCalculator taxCalculator = new TaxCalculator();

        BigDecimal result = taxCalculator.getPriceWithoutTax(new BigDecimal(18.15), new BigDecimal(21));

        assertEquals(15, result.doubleValue(), 0);
    }

    @Test
    public void given_price_without_tax_inluded_then_we_want_price_with_tax() {
        TaxCalculator taxCalculator = new TaxCalculator();

        BigDecimal result = taxCalculator.getPriceWithTax(new BigDecimal(90.91), new BigDecimal(10));

        assertEquals(100.00099999999999, result.doubleValue(), 0);
    }

    @Test
    public void given_price_without_tax_inluded_then_we_want_price_with_tax_case2() {
        TaxCalculator taxCalculator = new TaxCalculator();

        BigDecimal result = taxCalculator.getPriceWithTax(new BigDecimal(15), new BigDecimal(21));

        assertEquals(18.15, result.doubleValue(), 0);
    }

    @Test
    public void given_price_without_tax_included_then_we_want_the_price_with_tax_and_rounded() {
        TaxCalculator taxCalculator = new TaxCalculator();
        RoundUtil roundUtil = new RoundUtil();

        BigDecimal price = new BigDecimal(84.54545455);

        BigDecimal priceWithTax = taxCalculator.getPriceWithTax(price, new BigDecimal(10));
        BigDecimal result = roundUtil.round(priceWithTax);

        assertEquals(93.00, result.doubleValue(), 0);
    }

    @Test
    public void given_price_without_tax_included_then_we_want_the_price_with_tax_and_rounded_case2() {
        TaxCalculator taxCalculator = new TaxCalculator();
        RoundUtil roundUtil = new RoundUtil();

        BigDecimal price = new BigDecimal(3.30578512);

        BigDecimal result = taxCalculator.getPriceWithTax(price, new BigDecimal(21));
        result = roundUtil.round(result);

        assertEquals(4.00, result.doubleValue(), 0);
    }

    @Test
    public void given_price_with_tax_included_then_apply_getPriceWithoutTax_and_getPriceWithTax_must_return_the_initial_value() {
        TaxCalculator taxCalculator = new TaxCalculator();
        RoundUtil roundUtil = new RoundUtil();

        BigDecimal result = taxCalculator.getPriceWithoutTax(new BigDecimal(11.10), new BigDecimal(10));
        result = taxCalculator.getPriceWithTax(new BigDecimal(result.doubleValue()), new BigDecimal(10));
        result = roundUtil.round(result);

        assertEquals(11.10, result.doubleValue(), 0);
    }

    @Test
    public void given_price_with_tax_included_then_apply_getPriceWithoutTax_and_getPriceWithTax_must_return_the_initial_value_case2() {
        TaxCalculator taxCalculator = new TaxCalculator();
        RoundUtil roundUtil = new RoundUtil();

        BigDecimal result = taxCalculator.getPriceWithoutTax(new BigDecimal(7.99), new BigDecimal(21));
        result = taxCalculator.getPriceWithTax(new BigDecimal(result.doubleValue()), new BigDecimal(21));
        result = roundUtil.round(result);

        assertEquals(7.99, result.doubleValue(), 0);
    }

    @Test
    public void given_price_with_tax_included_then_apply_getPriceWithoutTax_and_getPriceWithTax_must_return_the_initial_value_case3() {
        TaxCalculator taxCalculator = new TaxCalculator();
        RoundUtil roundUtil = new RoundUtil();

        BigDecimal result = taxCalculator.getPriceWithoutTax(new BigDecimal(7.20), new BigDecimal(21));
        result = taxCalculator.getPriceWithTax(new BigDecimal(result.doubleValue()), new BigDecimal(21));
        result = roundUtil.round(result);

        assertEquals(7.20, result.doubleValue(), 0);
    }

    @Test
    public void given_price_with_tax_included_then_apply_getPriceWithoutTax_and_getPriceWithTax_must_return_the_initial_value_case4() {
        TaxCalculator taxCalculator = new TaxCalculator();
        RoundUtil roundUtil = new RoundUtil();

        BigDecimal result = taxCalculator.getPriceWithoutTax(new BigDecimal(7.0), new BigDecimal(21));
        result = taxCalculator.getPriceWithTax(new BigDecimal(result.doubleValue()), new BigDecimal(21));
        result = roundUtil.round(result);

        assertEquals(7.0, result.doubleValue(), 0);
    }
}
