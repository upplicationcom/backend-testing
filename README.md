## Training Backend Upplication

Welcome! This is an exercise for [backend developers](http://backend.upplication.com)
We hope you have fun and do your best :)

### How its work?

1. Fork this repo
2. Apply your solution
3. Review and... review again. Try to keep special attention on details.
4. Make a PR with a clear description and the time you spent solving it.
5. Wait for our response :)

Thanks for taking the time to take this little test with us.

### About this exercise

We have two classes not implemented: `com.upplication.training.testing.RoundUtil` & `com.upplication.training.testing.TaxCalculator`
and a testing class: `com.upplication.training.testing.TaxCalculatorTest`
We want you to solve the tests in TaxCalculatorTest (all green!)

Apply your solution in about ~ 10 min.

That's all, *trust yourself* and try to balance your solution with the time you have.



